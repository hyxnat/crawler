require_relative 'test_helper'

describe Page do
  before do
    URI.stubs(:join).returns(Application.root + '/data/sample.html')
    @parser = InputParser.new(Application.root + '/data/contact_us.html')
  end

  describe 'result' do
    it 'should puts page results' do
      $stdout.expects(:puts).twice #for sample and contact us
      @parser.result
    end
  end
end
