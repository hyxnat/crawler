require_relative 'test_helper'

describe UrlValidator do
  describe 'valid?' do
    it 'should return true' do
      UrlValidator.new('http://test.com').valid?.must_equal true
    end

    it 'should return true' do
      UrlValidator.new('ht:/test.com').valid?.must_equal false
    end
  end
end
