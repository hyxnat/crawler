require_relative 'test_helper'

describe Page do
  before do
    @page = Page.new(Application.root + '/data/home.html')
  end

  describe 'hrefs' do
    it 'should return page links' do
      @page.hrefs.must_equal ["/contact_us", "/about_us", "/sample"]
    end
  end
end
