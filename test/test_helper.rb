require 'bundler/setup'
require 'minitest/spec'
require 'minitest/autorun'
require 'minitest/reporters'
require 'mocha/mini_test'

require_relative '../lib/crawler'

module Application
  def self.root
    File.expand_path('../', __FILE__)
  end
end
