require_relative 'test_helper'

describe Page do
  before do
    URI.stubs(:join).returns(Application.root + '/data/sample.html')
    @page = PagesBuilder.new(Application.root + '/data/home.html')
  end

  describe 'execute' do
    it 'should return all links' do
      result = @page.execute
      result.find { |url| url.include? 'crawler/test/data/home.html' }.wont_be :empty?
      result.find { |url| url.include? 'crawler/test/data/sample.html' }.wont_be :empty?
    end
  end
end
