class PagesBuilder
  MAXIMUM_NO_PAGES = 50

  def initialize(level_1_url)
    @level_1_url = level_1_url
    @level_1_page = Page.new(level_1_url)
  end

  def execute
    level_2_urls.each do |url|
      break if @urls.size >= MAXIMUM_NO_PAGES
      hrefs = Page.new(url).hrefs
      @urls += hrefs.map { |href| absolute_url(href) }.uniq
    end
    @urls.uniq.take(MAXIMUM_NO_PAGES)
  end

  private

  def level_2_urls
    @urls = [@level_1_url] + @level_1_page.hrefs.map { |href| absolute_url(href) }
  end

  def root_url
    @root_url ||= @level_1_url.chomp(URI.parse(@level_1_url).path)
  end

  def absolute_url(href)
    URI.join(root_url, href).to_s
  end
end
