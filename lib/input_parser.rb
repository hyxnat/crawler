class InputParser
  NO_OF_THREADS = 4

  def initialize(url)
    @mutex = Mutex.new
    @pages = PagesBuilder.new(url).execute
  end

  def result
    threads = []
    current_page = nil

    NO_OF_THREADS.times do
      threads << Thread.new do
        until @pages.empty? do
          @mutex.synchronize { current_page = @pages.pop rescue nil }
          if current_page
            html = Page.new(current_page).content rescue nil
            puts "#{html.title} - #{html.css('input').size} inputs" if html
          end
        end
      end
    end

    threads.map(&:join)
  end
end
