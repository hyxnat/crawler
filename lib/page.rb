class Page
  MAXIMUM_NEXT_LEVEL_PAGES = 49

  attr_reader :content

  def initialize(href)
    @content = Nokogiri::HTML(open(href))
  end

  def hrefs
    hrefs = content.css('a').map { |link| link.attribute('href').to_s }
    hrefs.uniq.delete_if(&:empty?).select { |href| href !~ /^#/ }.take(MAXIMUM_NEXT_LEVEL_PAGES)
  end
end
