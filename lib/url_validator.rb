class UrlValidator
  def initialize(url)
    @url = url
  end

  def valid?
    !(@url =~ URI::regexp(%w(http https))).nil?
  end
end
