require 'open-uri'
require 'nokogiri'

require_relative 'url_validator'
require_relative 'input_parser'
require_relative 'pages_builder'
require_relative 'page'

class Crawler
  def initialize(url)
    @url = url
  end

  def result
    return 'Please enter a valid Url' unless valid_url?
    InputParser.new(@url).result
  end

  private

  def valid_url?
    UrlValidator.new(@url).valid?
  end
end
